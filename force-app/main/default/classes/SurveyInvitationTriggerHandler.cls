public class SurveyInvitationTriggerHandler {
    public static void updateSurveyScore(SurveyInvitation si){
        Decimal score=0;
        List<SurveyQuestionResponse> sqrList=[SELECT Id, ResponseId, QuestionId, InvitationId, ResponseValue FROM 
                                              SurveyQuestionResponse where InvitationId =:si.Id];
        //获取客户满意度分值
        Map<String,SatisfactionScore__c > chMap=SatisfactionScore__c.getall();
        for(SurveyQuestionResponse sqi:sqrList){
            if(chMap.get(sqi.ResponseValue)!=null){
                score+=chMap.get(sqi.ResponseValue).Score__c;
            }
        }
 		
        si.SurveyScore__c=score;
        
    }
}