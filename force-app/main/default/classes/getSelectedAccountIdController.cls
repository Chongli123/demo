public class getSelectedAccountIdController {
    public Integer selectedNumber{set;get;}
    public Set<String> accountIdList{set;get;}
    public ApexPages.StandardSetController setAccount{set;get;}
    public List<Contact> conList{set;get;}
    public SObject currentUrl{set;get;}
    public PageReference page{set;get;}
    public SendSurveys__c  Survey{set;get;}
    public getSelectedAccountIdController(ApexPages.StandardController controller){
       
    }
    public getSelectedAccountIdController(ApexPages.StandardSetController controller){
        currentUrl=controller.getRecord();
        setAccount=controller;
        selectedNumber=controller.getSelected().size();
        accountIdList=new Set<String>();
        for(sobject str:controller.getSelected()){
            accountIdList.add(str.Id);
        }
        
    }
    
    public PageReference initAction(){
       PageReference page= getAccountContact(accountIdList);
        return page;
    }
    
    public PageReference getAccountContact(Set<String> accountIdList){
        conList=[Select Id, AccountId from Contact where AccountId in:accountIdList];
        for(Contact con:conList){
            con.IsSendSurvey__c =true;
        }
        update conList;
        //page=new PageReference('/lightning/o/Account/list?filterName=00B2x0000067WhlEAE');
        return null;
    }
}