public without sharing class weChatRestResourceController {
    //获取Access_Token的接口
    public static string url{set;get;}
    //用户关注时新建用户信息
    public static void userSubscribe(Map<String,String> bodyMap){
        printlog(String.valueOf(bodyMap));
        String ToUserName=bodyMap.get('ToUserName');
        String FromUserName=bodyMap.get('FromUserName');
        url=[Select Id,Name, Access_Token_Interface__c  from weChatAccount__c Where Name =:ToUserName].Access_Token_Interface__c;
        String Access_Token=getAccessToken(url);
        //获取用户详细信息接口
        string userInfoInterface='https://api.weixin.qq.com/cgi-bin/user/info?access_token='+Access_Token+'&openid='+FromUserName+'&lang=zh_CN';
        system.debug('Access_Token=='+Access_Token);
        system.debug('userInfoInterface=='+userInfoInterface);
        userInfo userInfo=getUserInfo(userInfoInterface);
        system.debug('userInfo=='+userInfo);
        //打印日志
        weChatUser__c  wu = new weChatUser__c ();
        	wu.Name=userInfo.nickname;
        	wu.weChatOpenid__c=FromUserName;
        	wu.IsSubscribed__c =true;
        	wu.weChatAccount__c=getWeChatAccountId(ToUserName)!=null ?getWeChatAccountId(ToUserName):null;
            wu.City__c=userInfo.city;
        	wu.Country__c=userInfo.country;
            wu.HeadImgurl__c=userInfo.headimgurl;
            wu.Language__c=userInfo.language;
            wu.Province__c=userInfo.province;
            wu.Sex__c=userInfo.sex==1?'男':'女';
            wu.subscribe_scene__c=userInfo.subscribe_scene;
            wu.Subscribe_Time__c=String.valueOf(userInfo.subscribe_time);
            wu.Unionid__c=userInfo.unionid;
       		upsert wu weChatOpenid__c;
    }
    
    //用户取消关注时更新用户信息
    public static void userUnSubscribe(Map<String,String> bodyMap){
        //打印日志
        List<weChatUser__c>  wuList =[Select Id,weChatOpenid__c,IsSubscribed__c from weChatUser__c where weChatOpenid__c=:bodyMap.get('FromUserName')];
        if(wuList.size()>0){
           wuList[0].IsSubscribed__c=false;
           update wuList[0];
        } 
    }
    //解析XML内容
    public static Map<String,String> parseXml(String xmlString){
        Map<String,String> name_Text = new Map<String,String>();
        XmlStreamReader  doc = new XmlStreamReader(xmlString);
        Boolean isSafeToGetNextXmlElement= true;
        String localName='';
        String getText='';
        while(isSafeToGetNextXmlElement){
            if(doc.getEventType() == XmlTag.START_ELEMENT && doc.getLocalName()!='xml'){
               	localName=doc.getLocalName();
            }else if (doc.getEventType() == XmlTag.CHARACTERS) {
                getText=doc.getText(); 
            }
            
            if(localName !=''){
                name_Text.put(localName,getText); 
            }
            if (doc.hasNext()) {
                doc.next();
            } else {
                isSafeToGetNextXmlElement = false;
                break;
            }
        }
        return name_Text;
    }
    
    //打印日志
    public static void printlog(String body){
       	weChatLog__c  wcl=new weChatLog__c ();
        wcl.Info__c =body;
       	insert wcl; 
    }
    
    //获取微信公众号CRM Id
    public static String getWeChatAccountId(String toUserName){
        List<weChatAccount__c > waList=[Select Id,Name, Access_Token_Interface__c  from weChatAccount__c Where Name =:toUserName];
        if(waList.size()>0){
            return waList[0].Id;
        }else{
            return null;
        }
    }
    //获取Access_Token
    public static String getAccessToken(String url){
        Http htp=new Http();
        HttpRequest req=new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(url);
        HttpResponse res=htp.send(req);
        Map<String,Object> bodyMap=(Map<String,Object>)JSON.deserializeUntyped(res.getBody());
        system.debug('access_token='+bodyMap.get('access_token'));
        return String.valueOf(bodyMap.get('access_token'));
    }
    //获取用户详细信息
    public static userInfo getUserInfo(String url){
        Http htp=new Http();
        HttpRequest req=new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(url);
        HttpResponse res=htp.send(req);
        system.debug('res.getBody()==='+res.getBody());
        //Map<String,Object> bodyMap=(Map<String,Object>)JSON.deserializeUntyped(res.getBody());
        //system.debug('bodyMap==='+bodyMap);
        userInfo userIf=(userInfo)JSON.deserialize(res.getBody(),userInfo.class);
        return userIf;
        
    }
    
    public static void upsertUserInfo(String toUserName){
        
    }
    
    class userInfo{
        Integer subscribe{set;get;} 
    	String openid{set;get;} 
        String nickname{set;get;} 
     	Integer sex{set;get;} 
     	String language{set;get;} 
     	String city{set;get;} 
     	String province{set;get;} 
     	String country{set;get;} 
     	String headimgurl{set;get;}
     	Integer subscribe_time{set;get;}
     	String unionid{set;get;}
     	String remark{set;get;}
     	Integer groupid{set;get;}
     	List<String> tagid_list{set;get;}
     	String subscribe_scene{set;get;}
     	Integer qr_scene{set;get;}
     	String qr_scene_str{set;get;}
    }
    
}