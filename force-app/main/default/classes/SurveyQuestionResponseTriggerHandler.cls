public class SurveyQuestionResponseTriggerHandler {
	public static void updateSurveyScore(List<SurveyQuestionResponse__c> siList){
        SurveyTaker__c st=[Select id,name,AccountScore__c from SurveyTaker__c where Id=:siList[0].SurveyTaker__c];
        Decimal score=0;
        //List<SurveyQuestionResponse__c > sqrList=[SELECT Id,Name, Response__c  FROM SurveyQuestionResponse__c  where SurveyTaker__c =:si.Id];
        //获取客户满意度分值
        Map<String,SatisfactionScore__c > chMap=SatisfactionScore__c.getall();
        for(SurveyQuestionResponse__c sqi:siList){
            if(chMap.get(sqi.Response__c )!=null){
                score+=chMap.get(sqi.Response__c ).Score__c;
            }
        }
        st.AccountScore__c =score;
        update st;
        
    }
}