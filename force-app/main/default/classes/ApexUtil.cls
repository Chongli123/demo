public without sharing class ApexUtil {
    public static List<String> getPicklistValue(String objectName,String fieldName){
        //objectName = 'Contact';
        //fieldName ='LeadSource';
        List<String> valList =new  List<String>(); 
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            //System.debug(pickListVal.getLabel() +' '+pickListVal.getValue());
            valList.add(pickListVal.getValue());
        }  
        return valList;
    }
}