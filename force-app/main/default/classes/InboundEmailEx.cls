global Class InboundEmailEx implements Messaging.InboundEmailHandler {

global  Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope  env){
    
    system.debug('email===='+email);
        system.debug('binaryAttachments===='+email.binaryAttachments);
    weChatLog__c  wc=new weChatLog__c ();
    wc.Info__c =String.valueOf(email);
    insert wc;
    system.debug('env===='+env);
    Messaging.InboundEmailResult res= new Messaging.InboundEmailResult();
        try{
            /*case c= new case();
            c.subject= email.subject;
            c.Priority='High';
            c.Origin='Email';
            c.Status='new';
            c.Description= email.plainTextBody;
            string emailadd= email.fromAddress;
            contact con=[select id from contact where email=:email.fromAddress limit 1];
            c.ContactId=con.id;
            insert c;
            res.success=true;*/
            EmailMessage  em=new EmailMessage ();
            em.Subject=email.subject;
            em.FromAddress=email.fromAddress; 
            //em.HasAttachment='';
            em.Headers ='';
            em.HtmlBody =email.plainTextBody;
            em.ToAddress ='';
            em.RelatedToId ='0012x000008pkd3AAA';
            insert em;
            List<Attachment> acctList=new List<Attachment>();
            for(Integer i=0;i<email.binaryAttachments.size();i++){
                Attachment  acct=new Attachment();
                acct.Body=email.binaryAttachments[i].body;
                acct.ParentId=em.Id;
                acct.Name=email.binaryAttachments[i].fileName;
                acctList.add(acct);
            }
            if(acctList.size()>0){
                insert acctList;
            }
            
            }
        catch(Exception e){
            res.success=false;
            system.debug('error===='+e.getMessage());
        }           
    return res;
    }
}