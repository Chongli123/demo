public class MassAddSurveyParticipantMemberController {
    @AuraEnabled
    public static List<Account> getAccountInfo(String recordId){
        List<Account> accList=[Select Id,Name from Account];
        return accList;
    }
    
    
	@AuraEnabled
    public static List<Contact> getContactInfo(String recordId){
        List<ParticipantMenber__c> pmList=[Select Id,Contact__c from ParticipantMenber__c where SendSurvey__c=:recordId];
        Set<String> strList=new Set<String>();
        for(ParticipantMenber__c pm:pmList){
            strList.add(pm.Contact__c);
        }
        List<Contact> conList=[Select Id,Name,AccountId from Contact where Id not in:strList];
        return conList;
    }
    @AuraEnabled
    public static List<Contact> reGetContactInfo(String recordId,List<String> accList){
        List<ParticipantMenber__c> pmList=[Select Id,Contact__c from ParticipantMenber__c where SendSurvey__c=:recordId];
        Set<String> strList=new Set<String>();
        for(ParticipantMenber__c pm:pmList){
            strList.add(pm.Contact__c);
        }
        List<Contact> conList=[Select Id,Name,AccountId from Contact where Id not in:strList and AccountId in:accList];
        return conList;
    }
    
    @AuraEnabled
    public static string createParticipantMenbers(String recordId,List<String> conIdList){
        List<ParticipantMenber__c> pmList=new List<ParticipantMenber__c>();
        for(String str:conIdList){
            ParticipantMenber__c pm=new ParticipantMenber__c();
            pm.Contact__c=str;
            pm.SendSurvey__c =recordId;
            pmList.add(pm);
        }
        if(pmList.size()>0){
           insert pmList;
           return 'Success';
        }else{
           return ''; 
        }
            
        
    }
    
    
}