public without sharing class httpUtil {
    public static string httpPost(String method,String endpoint,String body){
        Http htp =new Http();
        HttpRequest req=new HttpRequest();
        req.setMethod(method);
        req.setEndpoint(endpoint);
        req.setBody(body);
        HttpResponse res=htp.send(req);
        return res.getBody();
    }
}