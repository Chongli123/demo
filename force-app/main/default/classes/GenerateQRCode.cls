public without sharing class GenerateQRCode {
	@AuraEnabled
    public static String generateCTempQRCode(String recordId){
        system.debug('recordId===='+recordId);
        String weChatAccountId='a0N2x000000xfYuEAI';
      	String getTokenUrl=[Select Id,Name, Access_Token_Interface__c  from weChatAccount__c Where Id =:weChatAccountId].Access_Token_Interface__c;
        String Access_Token=weChatRestResourceController.getAccessToken(getTokenUrl); 
        String url='https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token='+Access_Token;
        String body='{"expire_seconds": 604800, "action_name": "QR_STR_SCENE", "action_info": {"scene": {"scene_str": "'+recordId+'"}}}';
        String response=httpUtil.httpPost('POST',url,body);
        parseResponse re=(parseResponse)JSON.deserialize(response, parseResponse.class);
        //system.debug('re='+EncodingUtil.UrlEncode(re.ticket,'UTF-8'));
        Campaign cam=[Select Id from Campaign where id=:recordId];
        //cam.QRCodeAddress__c='https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='+re.ticket;
        update cam;
        return 'Success';
    }
    
    class parseResponse{
        String ticket{set;get;}
        String expire_seconds{set;get;}
        String url{set;get;}
    }
}