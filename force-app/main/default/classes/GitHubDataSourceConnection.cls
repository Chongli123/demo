/**
	*定义要支持的到GitHub REST API v3的连接,查询GitHub档案。
	*扩展数据源。要启用的连接类 Salesforce同步外部系统的模式及处理对外部数据的查询和查册。
 **/
global class GitHubDataSourceConnection extends DataSource.Connection {
    private DataSource.ConnectionParams connectionInfo;
    /**
     *   构造函数GitHubDataSourceConnection
     **/
    global GitHubDataSourceConnection(DataSource.ConnectionParams connectionInfo) {
        this.connectionInfo = connectionInfo;
    }
   /* 
    *调用从外部查询和获取结果用于SOQL查询、列表视图和详细页面的系统对象关联的外部对象外部数据源。queryContext
	*参数表示要运行的查询针对外部系统中的一个表。返回一个行列表作为查询结果。比如单独打开一条记录的时候查询数据
    */
    override global DataSource.TableResult query(DataSource.QueryContext context) {
        system.debug('context='+context);
        DataSource.Filter filter = context.tableSelection.filter;
        system.debug('filter='+filter);
        String url;
        if (filter != null) {
            String thisColumnName = filter.columnName;
            if (thisColumnName != null && (thisColumnName.equals('ExternalId') || thisColumnName.equals('login')))
                url = 'https://api.github.com/users/'+ filter.columnValue;
            else
                url = 'https://api.github.com/users';
        } else {
            url = 'https://api.github.com/users';
        }

        /**
         * 筛选、排序和应用limit和offset子句。
         **/
        List<Map<String, Object>> rows =DataSource.QueryUtils.process(context, getData(url));
        system.debug('rows='+rows);
        return DataSource.TableResult.get(true, null,context.tableSelection.tableSelected, rows);
    }

    /**
     *   定义外部系统的模式。管理员单击“验证与同步”时调用在用户界面为外部数据源。初始化数据
     **/
    override global List<DataSource.Table> sync() {
        List<DataSource.Table> tables =new List<DataSource.Table>();
        List<DataSource.Column> columns;
        columns = new List<DataSource.Column>();
        // Defines the indirect lookup field. (For this to work,
        // make sure your Contact standard object has a
        // custom unique, external ID field called github_username.)
        // 定义间接查找字段。(为了这个工作，确保你的联系人标准对象有自定义唯一的外部ID字段，名为github_username。)
        columns.add(DataSource.Column.indirectLookup('login', 'Contact', 'github_username__c'));
        columns.add(DataSource.Column.text('id', 255));
        columns.add(DataSource.Column.text('name',255));
        columns.add(DataSource.Column.text('company',255));
        columns.add(DataSource.Column.text('bio',255));
        columns.add(DataSource.Column.text('followers',255));
        columns.add(DataSource.Column.text('following',255));
        columns.add(DataSource.Column.url('html_url'));
        columns.add(DataSource.Column.url('DisplayUrl'));
        columns.add(DataSource.Column.text('ExternalId',255));
        tables.add(DataSource.Table.get('githubProfile','login',columns));
        return tables;
    }

    /**
     * 	 调用进行全文搜索并从外部系统获取SOSL查询和Salesforce的结果全球搜索。
     *   SearchContext参数表示要在外部表上运行的查询系统。返回SearchContext请求搜索的每个表的结果。初始化数据
     **/
    override global List<DataSource.TableResult> search(DataSource.SearchContext context) {
        system.debug('context='+context);
        List<DataSource.TableResult> results = new List<DataSource.TableResult>();
        for (Integer i =0;i< context.tableSelections.size();i++) {
            String entity = context.tableSelections[i].tableSelected;
            system.debug('entity===='+entity);
            // Search usernames
            String url = 'https://api.github.com/users/'+ context.searchPhrase;
            system.debug('entity===='+context.searchPhrase);
            system.debug('url===='+url);
            results.add(DataSource.TableResult.get( true, null, entity, getData(url)));
        }
        return results;
    }

    /**
     *   帮助器方法来解析数据。url参数是外部系统的url。从外部系统返回一个行列表。
     **/
    public List<Map<String, Object>> getData(String url) {
        system.debug('url='+url);
        String response = getResponse(url);
        // Standardize response string
        if (!response.contains('"items":')) {
            if (response.substring(0,1).equals('{')) {
                response = '[' + response  + ']';
            }
            response = '{"items": ' + response + '}';
        }
        List<Map<String, Object>> rows = new List<Map<String, Object>>();
        Map<String, Object> responseBodyMap = (Map<String, Object>)JSON.deserializeUntyped(response);
        /**
         *   Checks errors.
         **/
        Map<String, Object> error =(Map<String, Object>)responseBodyMap.get('error');
        if (error!=null) {
            List<Object> errorsList =(List<Object>)error.get('errors');
            Map<String, Object> errors =(Map<String, Object>)errorsList[0];
            String errorMessage = (String)errors.get('message');
            throw new DataSource.OAuthTokenExpiredException(errorMessage);
        }

        List<Object> fileItems = (List<Object>)responseBodyMap.get('items');
        if (fileItems != null) {
            for (Integer i=0; i < fileItems.size(); i++) {
                Map<String, Object> item =(Map<String, Object>)fileItems[i];
                rows.add(createRow(item));
            }
        } else {
            rows.add(createRow(responseBodyMap));
        }
		system.debug('rows===='+rows);
        return rows;
    }

    /**
     *   方法填充外部对象记录上的外部ID和显示URL字段 外部系统发送的'id'值。对象>项参数映射到表示一行的数据。
     *   返回带有外部ID和显示URL值的更新后的映射。解析接口返回的数据
     **/
    public Map<String, Object> createRow(Map<String, Object> item){
        Map<String, Object> row = new Map<String, Object>();
        for ( String key : item.keySet() ) {
            if (key == 'login') {
                row.put('ExternalId', item.get(key));
            } else if (key=='html_url') {
                row.put('DisplayUrl', item.get(key));
            }
            row.put(key, item.get(key));
        }
        return row;
    }

    /**
     *   帮助器方法来进行HTTP GET调用。url参数是外部系统的url。返回外部系统的响应。调用接口获取数据
     **/
    public String getResponse(String url) {
        system.debug('url='+url);
        // Perform callouts for production (non-test) results.
        Http httpProtocol = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndPoint(url);
        request.setMethod('GET');
        HttpResponse response = httpProtocol.send(request);
        return response.getBody();
    }
}