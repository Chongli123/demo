public without sharing class AccountOrgChartController {
    public string accountId{set;get;}
    public List<Account> acc{set;get;}
    public contactNumber cn{set;get;}
    public AccountOrgChartController(){
        accountId=ApexPages.currentPage().getParameters().get('id');
        if(accountId !=''){
           accountId='0012x000008pkd2AAA'; 
        }
        acc=[Select Id,Name from Account where Id=:accountId];
        Map<String,List<Contact>> mapList=new Map<String,List<Contact>>();
        List<Contact> conList=[Select Id ,Name, Job__c, AccountDept__c,Account.Name  from Contact where AccountId=:accountId];
        List<String> AccountDeptList=ApexUtil.getPicklistValue('Contact','AccountDept__c');
        List<String> JobList=ApexUtil.getPicklistValue('Contact','Job__c');
        if(conList.size()>0){
            for(Contact con :conList){
                for(String str:AccountDeptList){
                    system.debug('con.AccountDept__c==='+con.AccountDept__c);
                    system.debug('str==='+str);
                    if(con.AccountDept__c.contains(str)&& mapList.get(str)==null){
                        mapList.put(str,new List<Contact>{con});
                    }else if(con.AccountDept__c.contains(str) && mapList.get(str).size()>0){
                        mapList.get(str).add(con);
                    }
                }
                
                for(String str:JobList){
                    if(con.Job__c.contains(str) && mapList.get(str)==null){
                        mapList.put(str,new List<Contact>{con}); 
                    }else if(con.Job__c.contains(str) && mapList.get(str).size()>0){
                        mapList.get(str).add(con);
                    }
                }
            }
        }
        cn =new contactNumber();
        //
        cn.companyName=conList[0].Account.Name;
        //这个客户一共有多少联系人
        cn.totalNum=conList.size();
        //每个部门一共有多少联系人
        cn.xsbNum=mapList.get('销售部')!=null?mapList.get('销售部').size():0;
        cn.yfbNum=mapList.get('研发部')!=null?mapList.get('研发部').size():0;
        cn.cgbNum=mapList.get('采购部')!=null?mapList.get('采购部').size():0;
        cn.scbNum=mapList.get('生产部')!=null?mapList.get('生产部').size():0;
        cn.hrNum=mapList.get('人事部')!=null?mapList.get('人事部').size():0;
        //每个部门下每个职位一共有多少联系人
        cn.xszjNum=mapList.get('销售总监')!=null?mapList.get('销售总监').size():0;
        cn.xsjlNum=mapList.get('销售经理')!=null?mapList.get('销售经理').size():0;
        cn.xszgNum=mapList.get('销售主管')!=null?mapList.get('销售主管').size():0;
        cn.xsryNum=mapList.get('销售人员')!=null?mapList.get('销售人员').size():0;
        cn.yfzjNum=mapList.get('研发总监')!=null?mapList.get('研发总监').size():0;
        cn.yfjljNum=mapList.get('研发经理')!=null?mapList.get('研发经理').size():0;
        cn.yfzgNum=mapList.get('研发主管')!=null?mapList.get('研发主管').size():0;
        cn.yfryNum=mapList.get('研发人员')!=null?mapList.get('研发人员').size():0;
        cn.cgzjNum=mapList.get('采购总监')!=null?mapList.get('采购总监').size():0;
        cn.cgjlNum=mapList.get('采购经理')!=null?mapList.get('采购经理').size():0;
        cn.cgzgNum=mapList.get('采购主管')!=null?mapList.get('采购主管').size():0;
        cn.cgryNum=mapList.get('采购人员')!=null?mapList.get('采购人员').size():0;
        cn.sczjNum=mapList.get('生产总监')!=null?mapList.get('生产总监').size():0;
        cn.scjlNum=mapList.get('生产经理')!=null?mapList.get('生产经理').size():0;
        cn.sczgNum=mapList.get('生产主管')!=null?mapList.get('生产主管').size():0;
        cn.scryNum=mapList.get('生产人员')!=null?mapList.get('生产人员').size():0;
        cn.hrzjNum=mapList.get('HR总监')!=null?mapList.get('HR总监').size():0;
        cn.hrjlNum=mapList.get('HR经理')!=null?mapList.get('HR经理').size():0;
        cn.hrzgNum=mapList.get('HR主管')!=null?mapList.get('HR主管').size():0;
        cn.hrryNum=mapList.get('HR人员')!=null?mapList.get('HR人员').size():0;
            
            
    }
        
    @AuraEnabled
    public static contactNumber getOrgInfo(String recordId){
        //recordId='0012x000008pkd3AAA';
        Map<String,List<Contact>> mapList=new Map<String,List<Contact>>();
        List<Contact> conList=[Select Id ,Name, Job__c, AccountDept__c,Account.Name  from Contact where AccountId=:recordId];
        List<String> AccountDeptList=ApexUtil.getPicklistValue('Contact','AccountDept__c');
        List<String> JobList=ApexUtil.getPicklistValue('Contact','Job__c');
        if(conList.size()>0){
            for(Contact con :conList){
                for(String str:AccountDeptList){
                    system.debug('con.AccountDept__c==='+con.AccountDept__c);
                    system.debug('str==='+str);
                    if(con.AccountDept__c!=null && con.AccountDept__c.contains(str)&& mapList.get(str)==null){
                        mapList.put(str,new List<Contact>{con});
                    }else if(con.AccountDept__c!=null && con.AccountDept__c.contains(str) && mapList.get(str).size()>0){
                        mapList.get(str).add(con);
                    }
                }
                
                for(String str:JobList){
                    if(con.Job__c!=null && con.Job__c.contains(str) && mapList.get(str)==null){
                        mapList.put(str,new List<Contact>{con}); 
                    }else if(con.Job__c!=null && con.Job__c.contains(str) && mapList.get(str).size()>0){
                        mapList.get(str).add(con);
                    }
                }
            }
            contactNumber cn =new contactNumber();
            //公司名
            cn.companyName=conList[0].Account.Name;
            //CEO
            cn.CEO=mapList.get('CEO')!=null?mapList.get('CEO')[0].Name:'';
            //这个客户一共有多少联系人
            cn.totalNum=conList.size();
            //每个部门一共有多少联系人
            cn.xsbNum=mapList.get('销售部')!=null?mapList.get('销售部').size():0;
            cn.yfbNum=mapList.get('研发部')!=null?mapList.get('研发部').size():0;
            cn.cgbNum=mapList.get('采购部')!=null?mapList.get('采购部').size():0;
            cn.scbNum=mapList.get('生产部')!=null?mapList.get('生产部').size():0;
            cn.hrNum=mapList.get('人事部')!=null?mapList.get('人事部').size():0;
            //每个部门下每个职位一共有多少联系人
            cn.xszjNum=mapList.get('销售总监')!=null?mapList.get('销售总监').size():0;
            cn.xsjlNum=mapList.get('销售经理')!=null?mapList.get('销售经理').size():0;
            cn.xszgNum=mapList.get('销售主管')!=null?mapList.get('销售主管').size():0;
            cn.xsryNum=mapList.get('销售人员')!=null?mapList.get('销售人员').size():0;
            cn.yfzjNum=mapList.get('研发总监')!=null?mapList.get('研发总监').size():0;
            cn.yfjljNum=mapList.get('研发经理')!=null?mapList.get('研发经理').size():0;
            cn.yfzgNum=mapList.get('研发主管')!=null?mapList.get('研发主管').size():0;
            cn.yfryNum=mapList.get('研发人员')!=null?mapList.get('研发人员').size():0;
            cn.cgzjNum=mapList.get('采购总监')!=null?mapList.get('采购总监').size():0;
            cn.cgjlNum=mapList.get('采购经理')!=null?mapList.get('采购经理').size():0;
            cn.cgzgNum=mapList.get('采购主管')!=null?mapList.get('采购主管').size():0;
            cn.cgryNum=mapList.get('采购人员')!=null?mapList.get('采购人员').size():0;
            cn.sczjNum=mapList.get('生产总监')!=null?mapList.get('生产总监').size():0;
            cn.scjlNum=mapList.get('生产经理')!=null?mapList.get('生产经理').size():0;
            cn.sczgNum=mapList.get('生产主管')!=null?mapList.get('生产主管').size():0;
            cn.scryNum=mapList.get('生产人员')!=null?mapList.get('生产人员').size():0;
            cn.hrzjNum=mapList.get('HR总监')!=null?mapList.get('HR总监').size():0;
            cn.hrjlNum=mapList.get('HR经理')!=null?mapList.get('HR经理').size():0;
            cn.hrzgNum=mapList.get('HR主管')!=null?mapList.get('HR主管').size():0;
            cn.hrryNum=mapList.get('HR人员')!=null?mapList.get('HR人员').size():0;
            return cn;
        }else{
            return null;
        }
    }
    class contactNumber{
        @AuraEnabled 
        public String CEO{set;get;}
        @AuraEnabled 
        public String companyName{set;get;}
        //这个客户一共有多少联系人
        @AuraEnabled 
        public Integer totalNum{set;get;}
        //每个部门一共有多少联系人
        @AuraEnabled 
        public Integer xsbNum{set;get;}
        @AuraEnabled 
        public Integer yfbNum{set;get;}
        @AuraEnabled 
        public Integer cgbNum{set;get;}
        @AuraEnabled 
        public Integer scbNum{set;get;}
        @AuraEnabled 
        public Integer hrNum{set;get;}
        
        //每个部门下每个职位一共有多少联系人
        @AuraEnabled 
        public Integer xszjNum{set;get;}
        @AuraEnabled 
        public Integer xsjlNum{set;get;}
        @AuraEnabled 
        public Integer xszgNum{set;get;}
        @AuraEnabled 
        public Integer xsryNum{set;get;}
        @AuraEnabled 
        public Integer yfzjNum{set;get;}
        @AuraEnabled 
        public Integer yfjljNum{set;get;}
        @AuraEnabled 
        public Integer yfzgNum{set;get;}
        @AuraEnabled 
        public Integer yfryNum{set;get;}
        @AuraEnabled 
        public Integer cgzjNum{set;get;}
        @AuraEnabled 
        public Integer cgjlNum{set;get;}
        @AuraEnabled 
        public Integer cgzgNum{set;get;}
        @AuraEnabled 
        public Integer cgryNum{set;get;}
        @AuraEnabled 
        public Integer sczjNum{set;get;}
        @AuraEnabled 
        public Integer scjlNum{set;get;}
        @AuraEnabled 
        public Integer sczgNum{set;get;}
        @AuraEnabled 
        public Integer scryNum{set;get;}
        @AuraEnabled 
        public Integer hrzjNum{set;get;}
        @AuraEnabled 
        public Integer hrjlNum{set;get;}
        @AuraEnabled 
        public Integer hrzgNum{set;get;}
        @AuraEnabled 
        public Integer hrryNum{set;get;}
    }
}