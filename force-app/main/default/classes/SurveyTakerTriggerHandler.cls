public class SurveyTakerTriggerHandler {
    public static boolean flag=true;
	public static void updateSurveyScore(SurveyTaker__c si){
        Decimal score=0;
        List<SurveyQuestionResponse__c > sqrList=[SELECT Id,Name, Response__c  FROM SurveyQuestionResponse__c  where SurveyTaker__c =:si.Id];
        //获取客户满意度分值
        Map<String,SatisfactionScore__c > chMap=SatisfactionScore__c.getall();
        for(SurveyQuestionResponse__c sqi:sqrList){
            if(chMap.get(sqi.Response__c )!=null){
                score+=chMap.get(sqi.Response__c ).Score__c;
            }
        }
 		
        si.AccountScore__c =score;
        
    }
}