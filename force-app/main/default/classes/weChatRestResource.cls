@RestResource(urlMapping='/lcee/weChat')
global without sharing class weChatRestResource {
    @HttpGet
    global static void getWeChatInfo(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        // 微信加密签名  
        String signature=RestContext.request.params.get('signature');        
        // 时间戳  
        String timestamp=RestContext.request.params.get('timestamp');    
        // 随机数  
        String nonce=RestContext.request.params.get('nonce');       
        // 随机字符串  
        String echostr=RestContext.request.params.get('echostr'); 
        system.debug(signature+'====='+timestamp+'====='+nonce+'====');
        String endstr= '';
        if(checkSignature(signature,timestamp,nonce)){
            RestContext.response.addHeader('Content-Type', 'text/plain');
        	RestContext.response.responseBody = Blob.valueOf(echostr); 
        }

    }
    
    @HttpPost
    global static void getInfo(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String xmlString=req.requestBody.toString();
        weChatRestResourceController.printlog(String.valueOf(xmlString));
        xmlString=xmlString.remove('\n');
        
        Map<String,String> bodyMap=new Map<String,String>();
        bodyMap=weChatRestResourceController.parseXml(xmlString);
        //用户关注公众号 
        if(bodyMap.get('MsgType')=='event' && bodyMap.get('Event')=='subscribe'){
          weChatRestResourceController.userSubscribe(bodyMap);  
        }
        
        //用户取消关注公众号 
        if(bodyMap.get('MsgType')=='event' && bodyMap.get('Event')=='unsubscribe'){
          weChatRestResourceController.userUnSubscribe(bodyMap);  
        }
        
        //用户浏览网页 
        if(bodyMap.get('MsgType')=='event' && bodyMap.get('Event')=='VIEW]'){
          weChatInfoController.weChatOpenId=bodyMap.get('FromUserName');  
        }
        
        //点击菜单按钮 
        if(bodyMap.get('EventKey').contains('MUSIC')){
          RestContext.response.addHeader('Content-Type', 'text/plain');
          String content=weChatRestResourceResponseController.responseText(bodyMap);
          weChatRestResourceController.printlog('content===='+content);
          RestContext.response.responseBody = Blob.valueOf(content); 
        } 
        
    }
    
    public static boolean checkSignature(String signature, String timestamp, String nonce) {  
            String[] arr = new String[] { 'weixin', timestamp, nonce };  
            // 将token、timestamp、nonce三个参数进行字典序排序  
           // Arrays.sort(arr);  
            arr.sort();
            String content = '';  
            for (Integer i = 0; i < arr.size(); i++) {  
                content += arr[i];  
            }
            Blob hash = Crypto.generateDigest('SHA1',Blob.valueOf(content));
            String hexDigest = EncodingUtil.convertToHex(hash);
            String singUpperCase = hexDigest.toUpperCase();
            System.debug(singUpperCase +'===============');   
     
            // 将sha1加密后的字符串可与signature对比，标识该请求来源于微信  
            Boolean flag = singUpperCase != null ? singUpperCase.equals(signature.toUpperCase()) : false;
            system.debug(flag + '==============');
            return flag;      
     } 
    
}