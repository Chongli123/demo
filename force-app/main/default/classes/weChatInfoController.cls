public class weChatInfoController {
    public static string appid='wxed3c833ab7001186';//公众号唯一标识
    public static string weChatOpenId{set;get;}
    public static List<weChatUser__c> weChatUser{set;get;}
    public weChatInfoController(){
       
        
    }
    
    public static pageReference getWeChatUser(){ 
        string Url='https://open.weixin.qq.com/connect/oauth2/authorize?appid='+appid+'&redirect_uri='+
            	   'http://dingcanphp.applinzi.com/getUserInfo.php&response_type=code&scope='+
            	   'snsapi_userinfo&state=1#wechat_redirect';
        pageReference page=new pageReference(Url);
        /*system.debug('Apexpages.currentPage==='+Apexpages.currentPage());
        weChatRestResourceController.printlog('weChatOpenId='+weChatOpenId);
        weChatUser=[Select Id,Name,weChatOpenid__c,IsSubscribed__c,weChatAccount__c,
                    City__c,Country__c,HeadImgurl__c,Language__c,Province__c,Sex__c,subscribe_scene__c,
                    Subscribe_Time__c,Unionid__c from weChatUser__c where weChatOpenid__c=:weChatOpenId];*/
        return page;
    }
}