public class sendQuestionnaireSurveyController {
	@AuraEnabled
    public static string sendSurveyEmail(String recordId){
        try{
            List<ParticipantMenber__c > pmList=[Select Id,Contact__c,Contact__r.Name,Email__c,SendSurvey__r.CommunityId__c,SendSurvey__r.SurveyName__c,Survey_Invitation__c    from ParticipantMenber__c where SendSurvey__c =:recordId ];
            List<SurveyInvitation> siList=new List<SurveyInvitation>();
            for(ParticipantMenber__c pm:pmList){
                SurveyInvitation si=new SurveyInvitation();
                si.Name=pm.Contact__r.Name+'-调查邀请';
                si.ParticipantId=pm.Contact__c;
                si.SurveyId=pm.SendSurvey__r.SurveyName__c;
                si.CommunityId=pm.SendSurvey__r.CommunityId__c;
                si.OptionsAllowGuestUserResponse=true;
                siList.add(si);
            }
            if(siList.size()>0){
                //创建SurveyInvitation
                List<String> surveyLinkIdlist=new List<String>();
                Database.SaveResult[] ds=Database.insert(siList);
                //获取SurveyInvitation Id
                for(Database.SaveResult d:ds){
                    surveyLinkIdlist.add(d.getId());
                }
				
                Map<String,SurveyInvitation> ssi =new Map<String,SurveyInvitation>();
                for(SurveyInvitation si:[Select Id,ParticipantId,InvitationLink from SurveyInvitation where id in:surveyLinkIdlist ]){
                    ssi.put(si.ParticipantId,si);
                }
                //更新参与人员信息
                for(ParticipantMenber__c pm:pmList){
                    if(ssi.get(pm.Contact__c)!=null){
                        pm.Survey_Invitation__c=ssi.get(pm.Contact__c).Id;
                        pm.SurveyLink__c =ssi.get(pm.Contact__c).InvitationLink;
                    }
                }
                update pmList;
                sendEmail(pmList);
                system.debug('ds='+ds);   
            }
        	return 'success';
        }catch(Exception e){
			system.debug('error='+e.getMessage());            
            return  e.getMessage();
        }
            
    }
    
    public static void sendEmail(List<ParticipantMenber__c > pmList){
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        for(ParticipantMenber__c pm:pmList){
            String ebody='尊敬的'+pm.Contact__r.Name+' ：<br/>您好！首先感谢您及贵公司对山蒲公司的大力支持，'
                +'山蒲公司的发展和成功凝聚着贵公司的关怀。为了让山蒲能够为您提供更优质和更全面的服务，'
                +'特安排此项客户满意度调查，希望您能在百忙之中给予我们客观的评价。'
                +'如果您对本公司有其它要求和建议请一并提出，您的建议将是我们改善的动力，'
                +'我们将虚心听取并及时改进，谢谢配合！<br/>'
				+'<center>'+pm.SurveyLink__c+'</center><br/>谢谢<br/>'
                +'***请于12月10日前邮件或传真回传我公司，传真+86-578-3182969-XXXX<br/>浙江山蒲照明电器有限公司';
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSenderDisplayName('superSMART');//发件人名称
            email.setHtmlBody(ebody);//邮件内容
            email.setSubject('问卷调查');//主题
            email.setCharset('UTF-8');//编码方式
            email.setToAddresses(new list<String>{pm.Email__c});//收件人-参数为数组
            //email.setCcAddresses(s);//抄送人-参数为数组
            //email.setTargetObjectId(id);//用电子邮件模板时，必须
            emails.add(email);		
        }
		
		if(emails.size()>0&&!Test.isRunningTest())Messaging.sendEmail(emails);
    }
}