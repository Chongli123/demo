public without sharing class weChatRestResourceResponseController {
    //回复文字信息
    public static String responseText(Map<String,String> bodyMap){
        String ToUserName=bodyMap.get('ToUserName');
        String FromUserName=bodyMap.get('FromUserName');
        String CreateTime=System.now().format('YYYY-MM-dd HH:mm:ss');
        String text='该功能暂未开放';
        String[] arguments = new String[]{FromUserName,ToUserName,text};
        String responseMessageTemplate='<xml>'
                        +'<ToUserName><![CDATA[{0}]]></ToUserName>'
                        +'<FromUserName><![CDATA[{1}]]></FromUserName>'
                        +'<CreateTime>'+CreateTime+'</CreateTime>'
                      	+'<MsgType><![CDATA[text]]></MsgType>'
            			+'<Content><![CDATA[{2}]]></Content>'
                     +'</xml>';
        String results = String.format(responseMessageTemplate,arguments);
        return results;
    }
}