public class PersonalHomePageController {
    public PageReference pageRef{set;get;}
    public string userName{set;get;}
    public string passWord{set;get;}
    public string name{set;get;}
    public string email{set;get;}
    public string company{set;get;}
    public string job{set;get;}
    public string phone{set;get;}
    public List<UserDatabase__c>  userInfoList{set;get;}
    public PersonalHomePageController(){
        pageRef = ApexPages.currentPage();
        name=pageRef.getParameters().get('Name');
        email=pageRef.getParameters().get('email');
        company=pageRef.getParameters().get('Company');
        job=pageRef.getParameters().get('Job');
        phone=pageRef.getParameters().get('Phone');
        userName=pageRef.getParameters().get('userName');
        passWord=pageRef.getParameters().get('passWord');
        system.debug(pageRef);
        userInfoList=[Select Id,Name,UserName__c ,PassWord__c  from UserDatabase__c where UserName__c=:userName and PassWord__c=:passWord ];
        if(userInfoList.size()>0){
            returnSuccessPage();
        }else{
            returnFailedPage();
        }
        
    }
    
    
    public static pageReference  returnSuccessPage(){
        pageReference page=new pageReference('/');
        return page;
    }
    
    public static pageReference  returnFailedPage(){
        pageReference page=new pageReference('/');
        return page;
    }

}