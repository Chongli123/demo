import { LightningElement, api, wire ,track } from 'lwc';
import generateCTempQRCode from '@salesforce/apex/GenerateQRCode.generateCTempQRCode';
import { refreshApex } from '@salesforce/apex';
export default class GenerateQRCode extends LightningElement {
    @api recordId;
    @track contacts;
    @track error;

    @wire(generateCTempQRCode, { recordId: '$recordId'})
    wiredGetActivityHistory(value) {
        // Hold on to the provisioned value so we can refresh it later.
        this.wiredActivities = value;
        // Destructure the provisioned value 
        const { data, error } = value;
        if (data) { 
            alert('data='+ data);
        }else if (error) { 
            alert('error='+ error);
         }
    }
    handleLoad() {
        refreshApex(this.wiredActivities);
    }
}