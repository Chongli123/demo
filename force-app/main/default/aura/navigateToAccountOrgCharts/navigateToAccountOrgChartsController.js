({
	doInit : function(component, event, helper) {
		$A.get("e.force:navigateToComponent").setParams({
	        componentDef : "c:accountOrgCharts",
	        componentAttributes: {
	            recordId : component.get("v.recordId")
	        }
    	}).fire();
	}
})