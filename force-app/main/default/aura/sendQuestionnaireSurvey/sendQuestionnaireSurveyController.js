({
	handelOk : function(component, event, helper) {
		var action = component.get("c.sendSurveyEmail");
    	action.setParams({
    		"recordId" : component.get("v.recordId")
    	});
    	action.setCallback(this, function(response){
    		var responeVal = response.getReturnValue();
            if(responeVal=='success'){
                var showToast = $A.get("e.force:showToast");
                showToast.setParams({"type":"success","message":responeVal});
                showToast.fire();
                $A.get("e.force:closeQuickAction").fire();
            }else{
                var showToast = $A.get("e.force:showToast");
                showToast.setParams({"type":"error","message":responeVal});
                showToast.fire();
                $A.get("e.force:closeQuickAction").fire();
            }
    		
    	});
    	$A.enqueueAction(action);
	},
    handelCancel : function(component, event, helper) {
    	$A.get("e.force:closeQuickAction").fire();
    },
})