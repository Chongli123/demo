({
	getInit : function(component, event, helper) {
        //获取联系人信息
		var action = component.get("c.getContactInfo");
    	action.setParams({
    		"recordId" : component.get("v.recordId")
    	});
    	action.setCallback(this, function(response){
    		var responeVal = response.getReturnValue();
            var opts = [];
            if(responeVal.length>0){
                for(var p in responeVal){
                   opts.push({"class": "optionClass", label:responeVal[p].Name, value:responeVal[p].Id}); 
                }
                component.set("v.conList",opts);
            }
    	});
    	//$A.enqueueAction(action);
        
        //获取客户信息
        var action1 = component.get("c.getAccountInfo");
    	action1.setParams({
    		"recordId" : component.get("v.recordId")
    	});
    	action1.setCallback(this, function(response){
    		var responeVal = response.getReturnValue();
            var opts = [];
            if(responeVal.length>0){
                for(var p in responeVal){
                   opts.push({"class": "optionClass", label:responeVal[p].Name, value:responeVal[p].Id}); 
                }
                component.set("v.accList",opts);
            }
    	});
    	$A.enqueueAction(action1);
	},
    getInitContacts : function(component, event, helper) {
        component.set("v.loaded", true);
        //获取联系人信息
		var action = component.get("c.reGetContactInfo");
    	action.setParams({
    		"recordId" : component.get("v.recordId"),
            "accList" : component.get("v.selectedAccList"),
    	});
    	action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var responeVal = response.getReturnValue();
                var opts = [];
                if(responeVal.length>0){
                    for(var p in responeVal){
                        opts.push({"class": "optionClass", label:responeVal[p].Name, value:responeVal[p].Id}); 
                    }
                    component.set("v.conList",opts);
                }else{
                    component.set("v.conList",''); 
                }
                component.set("v.loaded", false);
            }else{
                var showToast = $A.get("e.force:showToast");
                showToast.setParams({
                    "type":"warning",
                    "message": "发生异常。" + response.getError()
                });
                showToast.fire();
                component.set("v.loaded", false);
            }  
    	});
    	$A.enqueueAction(action);
	}
})