({
	init : function(component, event, helper) {
        component.set("v.loaded", true);
		helper.getInit(component, event, helper);
        component.set("v.loaded", false);
	},
    Add : function(component, event, helper) {
		helper.getInitContacts(component, event, helper);
	},
    Save : function(component, event, helper) {
        if(component.get("v.selectedConList").length>0){
            component.set("v.loaded", true);
            var action = component.get("c.createParticipantMenbers");
            action.setParams({
                "recordId" : component.get("v.recordId"),
                "conIdList" : component.get("v.selectedConList"),
            });
            action.setCallback(this, function(response){
                var responeVal = response.getReturnValue();
                var showToast = $A.get("e.force:showToast");
                showToast.setParams({"type":"success","message":responeVal});
                showToast.fire();
                $A.get("e.force:closeQuickAction").fire();
            });
            $A.enqueueAction(action);
        }else{
            alert('你没有选择任何联系人');
        } 
    },
    SaveAndNew : function(component, event, helper) {
        if(component.get("v.selectedConList").length>0){
            component.set("v.loaded", true);
            var action = component.get("c.createParticipantMenbers");
            action.setParams({
                "recordId" : component.get("v.recordId"),
                "conIdList" : component.get("v.selectedConList"),
            });
            action.setCallback(this, function(response){
                var responeVal = response.getReturnValue();
                var showToast = $A.get("e.force:showToast");
                showToast.setParams({"type":"success","message":responeVal});
                showToast.fire();
				component.set("v.loaded", false);
                helper.getInit(component, event, helper);
            });
            $A.enqueueAction(action);
        }else{
            alert('你没有选择任何联系人');
        } 
    },
    Cancel : function(component, event, helper) {
    	$A.get("e.force:closeQuickAction").fire();
    },
})