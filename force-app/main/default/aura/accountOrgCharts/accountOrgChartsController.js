({
	doInit : function(component, event, helper) {
        var action = component.get("c.getOrgInfo");
    	action.setParam("recordId", component.get("v.recordId"));
    	action.setCallback(this, function(response){
    		var state = response.getState();
    		if(state == "SUCCESS"){
				var returnInfo=response.getReturnValue();
				if(returnInfo==null){
					var showToast = $A.get("e.force:showToast");
                    showToast.setParams({
                        "type":"warning",
                        "message": "该客户还没有联系人"
                    });
                    showToast.fire(); 
					$A.get("e.force:closeQuickAction").fire();
				}
                component.set("v.companyName",returnInfo.companyName);
                component.set("v.AccountOrgChartInfo",returnInfo);
                var datascource = {
                    'name': returnInfo.companyName,
                    'title':'共有'+returnInfo.totalNum+'位联系人',
                    'children':[{'name':'CEO' ,'title':'<B>'+returnInfo.CEO+'</b>',
                    'children':[
                    		{ 'name': '销售部', 'title': '<b>一共有'+returnInfo.xsbNum+'位联系人</b>',
                             'children': [{ 'name': '销售总监', 'title': '<span style="color:if({!returnInfo.xszjNum}>0,black,red)">'+returnInfo.xszjNum+'</span>位联系人', 
                                       'children': [{ 'name': '销售经理', 'title': '<span >'+returnInfo.xsjlNum+'</span>位联系人',
                                                     'children': [{ 'name': '销售主管', 'title': returnInfo.xszgNum+'位联系人',
                                                                       'children': [{ 'name': '销售人员', 'title': returnInfo.xsryNum+'位联系人'}] 
                                                                      }] 
                                                        }]
                                          }] 
                            },
                            { 'name': '研发部', 'title': '<b>一共有'+returnInfo.yfbNum+'位联系人</b>',
                             'children': [{ 'name': '研发总监', 'title': returnInfo.yfzjNum+'位联系人', 
                                           'children': [{ 'name': '研发经理', 'title': returnInfo.yfjljNum+'位联系人',
                                                         'children': [{ 'name': '研发主管', 'title': returnInfo.yfzgNum+'位联系人',
                                                                       'children': [{ 'name': '研发人员', 'title': returnInfo.yfryNum+'位联系人' }] 
                                                                      }] 
                                                        }]
                                          }] 
                            },
                            { 'name': '采购部', 'title': '<b>一共有'+returnInfo.cgbNum+'位联系人</b>',
                             'children': [{ 'name': '采购总监', 'title':returnInfo.cgzjNum+'位联系人', 
                                           'children': [{ 'name': '采购经理', 'title': returnInfo.cgjlNum+'位联系人',
                                                         'children': [{ 'name': '采购主管', 'title': returnInfo.cgzgNum+'位联系人',
                                                                       'children': [{ 'name': '采购人员', 'title': returnInfo.cgryNum+'位联系人' }] 
                                                                      }] 
                                                        }]
                                          }] 
                            },
                            { 'name': '生产部', 'title': '<b>一共有'+returnInfo.scbNum+'位联系人</b>',
                             'children': [{ 'name': '生产总监', 'title': returnInfo.sczjNum+'位联系人', 
                                           'children': [{ 'name': '生产经理', 'title': returnInfo.scjlNum+'位联系人',
                                                         'children': [{ 'name': '生产主管', 'title': returnInfo.sczgNum+'位联系人',
                                                                       'children': [{ 'name': '生产人员', 'title': returnInfo.scryNum+'位联系人' }] 
                                                                      }] 
                                                        }]
                                          }] 
                            },
                            { 'name': '人力资源', 'title': '<b>一共有'+returnInfo.hrNum+'位联系人</b>',
                             'children': [{ 'name': 'HR总监', 'title': returnInfo.hrzjNum+'位联系人', 
                                           'children': [{ 'name': 'HR经理', 'title': returnInfo.hrjlNum+'位联系人',
                                                         'children': [{ 'name': 'HR主管', 'title': returnInfo.hrzgNum+'位联系人',
                                                                       'children': [{ 'name': 'HR人员', 'title': returnInfo.hrryNum+'位联系人' }] 
                                                                      }] 
                                                        }]
                                          }] 
                            }
                    ]}]
                       
                };
                $('#chart-container').orgchart({
                    'data' : datascource, //数据
                    'nodeContent': 'title' //内容对应的字段
                });
    		}
    	});
		$A.enqueueAction(action);
                
	}
})