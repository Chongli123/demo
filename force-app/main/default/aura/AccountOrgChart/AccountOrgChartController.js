({
  init: function(component) {
    var action = component.get("c.getOrgInfo");
    action.setParam("recordId", component.get("v.recordId"));
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state == "SUCCESS") {
        var result = response.getReturnValue(); //2020/5/21   Spring Ji
        //component.set("v.conList",result);
        var items = [
          {
            label:
              result.companyName + "(一共有" + result.totalNum + "位联系人)",
            name: "1",
            expanded: true,
            items: [
              {
                label: "销售部(一共有" + result.xsbNum + "位联系人)",
                name: "2",
                expanded: true,
                items: [
                  {
                    label: "销售总监(" + result.xszjNum + "位联系人)",
                    name: "3",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "销售经理(" + result.xsjlNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "销售主管(" + result.xszgNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "销售人员(" + result.xsryNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  }
                ]
              },
              {
                label: "研发部(一共有" + result.yfbNum + "位联系人)",
                name: "2",
                expanded: true,
                items: [
                  {
                    label: "研发总监(" + result.yfzjNum + "位联系人)",
                    name: "3",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "研发经理(" + result.yfjljNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "研发主管(" + result.yfzgNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "研发人员(" + result.yfryNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  }
                ]
              },
              {
                label: "采购部(一共有" + result.cgbNum + "位联系人)",
                name: "2",
                expanded: true,
                items: [
                  {
                    label: "采购总监(" + result.cgzjNum + "位联系人)",
                    name: "3",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "采购经理(" + result.cgjlNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "采购主管(" + result.cgzgNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "采购人员(" + result.cgryNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  }
                ]
              },
              {
                label: "人事部(一共有" + result.hrNum + "位联系人)",
                name: "2",
                expanded: true,
                items: [
                  {
                    label: "HR总监(" + result.hrzjNum + "位联系人)",
                    name: "3",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "HR经理(" + result.hrjlNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "HR主管(" + result.hrzgNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "HR人员(" + result.hrryNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  }
                ]
              },
              {
                label: "生产部(一共有" + result.scbNum + "位联系人)",
                name: "2",
                expanded: true,
                items: [
                  {
                    label: "生产总监(" + result.sczjNum + "位联系人)",
                    name: "3",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "生产经理(" + result.scjlNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "生产主管(" + result.sczgNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  },
                  {
                    label: "生产人员(" + result.scryNum + "位联系人)",
                    name: "4",
                    expanded: true,
                    items: []
                  }
                ]
              }
            ]
          }
        ];
        component.set("v.items", items);
      }
    });
    $A.enqueueAction(action);
  },
  new: function() {
    alert("test");
  }
});