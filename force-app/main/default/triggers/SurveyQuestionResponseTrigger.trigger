trigger SurveyQuestionResponseTrigger on SurveyQuestionResponse__c (after insert) {
	SurveyQuestionResponseTriggerHandler.updateSurveyScore(trigger.new);
}