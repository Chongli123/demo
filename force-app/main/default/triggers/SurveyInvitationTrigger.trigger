trigger SurveyInvitationTrigger on SurveyInvitation (before update) {
    if(trigger.new[0].ResponseStatus=='Completed'){
        SurveyInvitationTriggerHandler.updateSurveyScore(trigger.new[0]);
    }
    
}